from dtos.board import BoardMove
from fastapi import status, HTTPException
from models.game_model import GameModel
from services.auth_service import AuthService


class GameService:

    @staticmethod
    def get_games(token: str):
        """
            Return history of games ordered by history
            :param token:
            :return: dict
        """
        user_id = AuthService.get_user_id_by_token(token)
        games = GameModel().get_games_by_user_id(user_id)
        games_response = {
            "response": games
        }
        if not games:
            games_response["detail"] = "There are no games"

        return games_response

    @staticmethod
    def create_game(token: str):
        """
            Creates new game board
            :param token:
            :return: dict
        """
        user_id = AuthService.get_user_id_by_token(token)
        games_data = GameModel().create_game(user_id)
        return {"response": games_data}

    @staticmethod
    def get_board(board_id: str, token: str):
        """
            Returns current board
            :param board_id
            :param token
            :return: dict
        """
        user_id = AuthService.get_user_id_by_token(token)
        current_board = GameModel().get_board(board_id, user_id)
        if current_board:
            return {"response": current_board}
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Game board not found",
        )

    @classmethod
    def make_move(cls, board_id: str, board_move: BoardMove, token: str):
        """
            Makes a move, if the game isn't completed, IA also makes play.
            :param board_move
            :param token
            :return: dict
        """
        if board_move.x < 0 or board_move.x > 2 or board_move.y < 0 or board_move.y > 2:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Game positions must be between 0 and 2",
            )

        user_id = AuthService.get_user_id_by_token(token)
        # @TODO Getting and saving a move for a board may lead to a race condition if simultaneous API calls are made
        game_model = GameModel().get_board(board_id, user_id)

        if not game_model:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Game ID not found",
            )

        if game_model['games'][0]['finished']:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Game is finished",
            )

        current_board = game_model['games'][0]['board']

        current_position_value = current_board[board_move.x][board_move.y]
        if current_position_value != ".":
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="The position given is already taken",
            )

        moves_made = []
        current_board[board_move.x][board_move.y] = "X"
        moves_made.append({
            "x": board_move.x,
            "y": board_move.y
        })

        game_finished = cls._has_won(current_board, board_move.x, board_move.y)
        if not game_finished:
            # Player has not won, IA plays
            current_board, x_position, y_position = cls._make_ia_move(current_board)

            moves_made.append({
                "x": x_position,
                "y": y_position
            })

            # Check if IA won
            game_finished = cls._has_won(current_board, x_position, y_position)

        GameModel().save_board(board_id, user_id, current_board, game_finished, moves_made)
        game_finished_label = "Game is finished" if game_finished else "Game is not finished"
        return {"response": current_board, "detail": game_finished_label}

    @staticmethod
    def is_game_finished(board: list):
        """
            Checks that the board has at least one empty position
            :param board:
            :return: boolean
        """
        for board_x in board:
            for board_y in board:
                if board[board_x][board_y] == ".":
                    return False
        return True

    @staticmethod
    def _has_won(board: list, x_position: int, y_position: int):
        """
            Checks if new move wins
            :param board:
            :param x_position:
            :param y_position:
            :return: boolean
        """
        current_player = board[x_position][y_position]

        # X positions
        for row in board:
            if all(x_position == current_player for x_position in row):
                return True

        # Y positions
        for col in range(len(board)):
            if all(board[row][col] == current_player for row in range(len(board))):
                return True

        # Diagonal
        if all(board[i][i] == current_player for i in range(len(board))):
            return True

        # Second Diagonal
        if all(board[i][len(board) - 1 - i] == current_player for i in range(len(board))):
            return True

        return False

    @staticmethod
    def _make_ia_move(board: list):
        """
            IA Plays the first empty position found
            :param board:
            :return: board
        """
        for x_index, board_x in enumerate(board):
            for y_index, board_y in enumerate(board_x):
                if board_y == ".":
                    board[x_index][y_index] = "O"
                    return board, x_index, y_index
