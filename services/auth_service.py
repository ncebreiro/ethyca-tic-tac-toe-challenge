from dtos.user import User
from fastapi import status, HTTPException
from models.auth_model import AuthModel

import datetime
import os
import hashlib
import jwt


class AuthService:

    TOKEN_HOURS_EXPIRATION = 24

    @classmethod
    def create_user(cls, user: User):
        """
            Creates user
            :param user
        """
        auth_model = AuthModel()
        user_already_exists = auth_model.get_user(user)
        if not user_already_exists:
            user.password = cls._hash_password(user)
            auth_model.create_user(user)
            return {"detail": "User created"}
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid credentials",
        )

    @classmethod
    def login_user(cls, user: User):
        """
            Login user
            :param user
            :return: token
        """
        auth_model = AuthModel()
        user_model = auth_model.get_user(user)
        hashed_password = cls._hash_password(user)
        if user_model and user_model['password'] == hashed_password:
            payload = {
                'id': str(user_model['_id']),
                'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=cls.TOKEN_HOURS_EXPIRATION)
            }
            token = jwt.encode(payload, os.getenv("JWT_SECRET_KEY"), algorithm='HS256')
            return {"token": token, "detail": "Login successful"}

        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid credentials",
        )

    @classmethod
    def get_user_id_by_token(cls, token: str):
        """
            Get user with a valid token
            :param token
            :return: user
        """
        decoded_payload = jwt.decode(token, os.getenv("JWT_SECRET_KEY"), algorithms='HS256')
        if decoded_payload:
            user_id = decoded_payload['id']
            return user_id

        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid token, log in again",
        )

    @staticmethod
    def _hash_password(user: User):
        """
            Returns password hashed
            :param user
            :return: str
        """
        password_bytes = user.password.encode('utf-8')
        password_hashed = hashlib.sha256(password_bytes).hexdigest()
        return password_hashed

