from db.connector import get_database
from dtos.user import User
from models.game_model import GameModel


class AuthModel:

    def __init__(self, user_instance=None):
        self._user_instance = user_instance or get_database()['users']

    def get_user(self, user: User):
        user_record = self._user_instance.find_one({'username': user.username})
        return user_record

    def get_user_by_id(self, user_id: str):
        user_record = self._user_instance.find_one({'_id': user_id})
        return user_record

    def login(self, user: User):
        user_record = self._user_instance.find_one({'username': user.username})
        return user_record

    def create_user(self, user: User):
        user_data = {
            'username': user.username,
            'password': user.password,
        }
        user_inserted = self._user_instance.insert_one(user_data)
        GameModel().create_basic_data(str(user_inserted.inserted_id))
