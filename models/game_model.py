from db.connector import get_database
from bson.objectid import ObjectId
from utils.encoder import ObjectIdEncoder
import json


class GameModel:

    def __init__(self, game_instance=None):
        self._game_instance = game_instance or get_database()['games']

    def get_games_by_user_id(self, user_id: str):
        game_records = self._game_instance.find_one({'user_id': user_id})
        games_found = game_records['games']
        game_formatted = json.loads(ObjectIdEncoder().encode(games_found)) if games_found else []
        return game_formatted

    def create_basic_data(self, user_id: str):
        self._game_instance.insert_one(
            {
                'user_id': user_id,
                'games': [],
            }
        )

    def create_game(self, user_id):
        new_board_data = {
            '_id': ObjectId(),  # @TODO check if this could lead to a problem
            'board': self._get_new_empty_board(),
            'moves': [],
            'finished': False,
        }
        self._game_instance.update_one(
            {'user_id': user_id},
            {'$push': {'games': new_board_data}}
        )
        return json.loads(ObjectIdEncoder().encode(new_board_data))

    def get_board(self, board_id: str, user_id: str):
        game_id = ObjectId(board_id)
        board = self._game_instance.find_one(
            {
                'user_id': user_id,
                'games': {'$elemMatch': {'_id': game_id}}
            },
            {
                'games.$': 1
            }
        )
        return json.loads(ObjectIdEncoder().encode(board))

    def save_board(self, board_id: str, user_id: str, board: list, finished: bool, moves_made: list):
        board_id_formatted = ObjectId(board_id)
        self._game_instance.update_one(
            {
                'user_id': user_id,
                'games': {'$elemMatch': {'_id': board_id_formatted}}
            },
            {
                '$set': {
                    'games.$.board': board,
                    'games.$.finished': finished
                },
                '$push': {
                    'games.$.moves': {'$each': moves_made}
                },
            }
        )

    @staticmethod
    def _get_new_empty_board():
        return [
            [".", ".", "."],
            [".", ".", "."],
            [".", ".", "."],
        ]
