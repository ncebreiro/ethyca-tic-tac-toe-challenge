<a name="readme-top"></a>

<!--
HOW TO USE:
This is an example of how you may give instructions on setting up your project locally.

Modify this file to match your project and remove sections that don't apply.

REQUIRED SECTIONS:
- Installation
- How to run
- Getting Started with APIs

-->

<div align="center">
  <!-- You are encouraged to replace this logo with your own! Otherwise you can also remove it. -->
  <img src="https://s3-recruiting.cdn.greenhouse.io/external_greenhouse_job_boards/logos/400/116/900/original/ethyca_logo_primary.png?1620392455" alt="logo" width="140"  height="auto" />
  <br/>

  <h3><b>Ethyca Technical Challenge README</b></h3>

</div>

<!-- TABLE OF CONTENTS -->

# 📗 Table of Contents

- [💻 Getting Started](#getting-started)
  - [Setup](#setup)
  - [Run Application](#usage)
  - [Application Usage](#application-usage)
- [📖 Delivery Information](#delivery-information)
- [👥 Authors](#authors)
- [🔭 Coming Up](#coming-up)

<!-- GETTING STARTED -->

## 💻 Getting Started <a id="getting-started"></a>

### Prerequisites

In order to run this project you need:

- Python3.X
- Virtualenvs for Python
- Mongodb client. For Macos:
```
brew tap mongodb/brew
brew install mongodb-community@7.0
```

### Setup

#### Virtualenv
- Create virtualenv
```
virtualenv fastapi_tic_tac_toe
```
- While using virtualenv install dependencies from project root.
```
pip install -r requirements.txt
```
- Copy .env.example. Replace secret and mongodb connection string.
```
cp .venv.example .venv
```


### Usage <a id="usage"></a>

#### Run application
To run the project, execute the following command:

```sh
 uvicorn main:app --reload
```

### Application Usage <a id="application-usage"></a>

- `post` to `/auth/user` to create a user.
- `get` to `/auth/login` to get a user's token.
- `get` to `/game/all` to retrieve current user's games. <i>Authorization token must be provided</i>.
- `post` to `/game` creates a new game board. <i>Authorization token must be provided</i>.
- `get` to `/game/{board_id}` gets specific board's information along with history of moves. <i>Authorization token must be provided</i>.
- `put` to `/game/move/{board_id}` make move at a game board. <i>Authorization token must be provided</i>.


<b>You can access to the Swagger interface/documentation while the application is running. Go to `/docs` to learn more.</b>

### 📖 **Delivery Information** <a id="delivery-information"></a>

- <i>"How much time you spent building the project"</i>: I have spent almost 5 hours approximately.
- <i>"Any assumptions you made"</i>: 
  - I assumed that users are needed given that I need to return historical data.
  - I didn't consider simultaneous API calls for a same user.
- <i>"Any trade-offs you made"</i>: 
  - I haven't done an intelligent IA.
  - I didn't add tests, docker, oauth nor linting tool since I wanted to spend more time on documentation and mongodb which I am not completely used to.
- <i>"Any special/unique features you added"</i>:
  - I tried spending time thinking in the structure of the project which I think is a key part at projects.
  - I added a few controls for data inputs, validations and throwing errors.
- <i>"Any feedback you have on this technical challenge"</i>: I liked the challenge because it's a very complete exercise that forced me to show a variety of tools for APIs. At the same time it has an interesting and low business level that caught me for more than four hours.
### 👤 **Author** <a id="authors"></a>

- Nicolas Cebreiro

<!-- COMING UP -->

## 🔭 COMING UP <a id="coming-up"></a>

- [ ] **Tests**
- [ ] **OAuth**
- [ ] **Docker**
- [ ] **Linting**

<p align="right">(<a href="#readme-top">back to top</a>)</p>
