from pymongo import MongoClient
import os


def get_database():
    connection_string = os.getenv("MONGODB_CONNECTION_STRING")
    client = MongoClient(connection_string)
    return client[os.getenv("MONGODB_DATABASE_NAME")]
