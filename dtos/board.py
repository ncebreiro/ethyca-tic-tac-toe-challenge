from pydantic import BaseModel


class BoardMove(BaseModel):
    x: int
    y: int
