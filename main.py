from dotenv import load_dotenv
from dtos.user import User
from dtos.board import BoardMove
from fastapi import Depends, FastAPI
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from typing import Annotated
from services.auth_service import AuthService
from services.game_service import GameService

app = FastAPI()
security = HTTPBearer()
load_dotenv()


@app.post("/auth/user")
def create_user(user: User):
    """
        Creates user
    """
    return AuthService.create_user(user)


@app.post("/auth/login")
def login_user(user: User):
    """
        Get user token
    """
    return AuthService.login_user(user)


@app.get("/game/all")
def get_all_games(credentials: Annotated[HTTPAuthorizationCredentials, Depends(security)]):
    """
        List user's games
    """
    print({"scheme": credentials.scheme, "credentials": credentials.credentials})
    return GameService.get_games(credentials.credentials)


@app.post("/game")
def create_game(credentials: Annotated[HTTPAuthorizationCredentials, Depends(security)]):
    """
        Creates new game board
    """
    return GameService.create_game(credentials.credentials)


@app.get("/game/{board_id}")
def get_game(board_id: str, credentials: Annotated[HTTPAuthorizationCredentials, Depends(security)]):
    """
        Get current positions and history of moves for specific game
    """
    return GameService.get_board(board_id, credentials.credentials)


@app.put("/game/move/{board_id}")
def game(board_id: str, board_move: BoardMove, credentials: Annotated[HTTPAuthorizationCredentials, Depends(security)]):
    """
        Make a move to a given board
    """
    return GameService.make_move(board_id, board_move, credentials.credentials)
